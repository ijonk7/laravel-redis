<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // $p = \RedisPhp::incr('p');
    // return $p;

    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/redis-1', [App\Http\Controllers\HomeController::class, 'tesRedis'])->name('redis.1');
Route::get('/cache-1', [App\Http\Controllers\CacheController::class, 'tesCache'])->name('cache.1');
