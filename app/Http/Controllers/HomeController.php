<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function tesRedis()
    {
        // \RedisPhp::set('kota', 'Papua');
        // dd(\RedisPhp::get('kota'));
        // \RedisPhp::flushall();

        // $data1 = User::all();

        // Kalau di get jadi object array dalam Collection
        // \RedisPhp::set('dataUser101', serialize($data1));
        // $data2 = unserialize(\RedisPhp::get('dataUser101'));

        // Kalau di get jadi object dalam array
        // \RedisPhp::set('data-user-2', json_encode($data1));
        // $data2 = json_decode(\RedisPhp::get('data-user-2'));

        // Kalau di get jadi string berformat object dalam array
        // $data1 = User::all()->toJson();
        // \RedisPhp::set('data-user-1', $data1);
        // $data2 = \RedisPhp::get('data-user-1');

        // dd(\RedisPhp::get('data-user'));

        // foreach ($data2 as $value) {
        //     echo $value->name . "<br>";
        // }

        // dd($data2);

        // \RedisPhp::del('data-user-22');

        // dd(\RedisPhp::exists('acara'));

        // \RedisPhp::append('data-user-22', serialize($data1));

        // $data3 = \RedisPhp::keys('data-user*');

        // $data3 = \RedisPhp::getrange('olahraga', 2, 4);
        // $data3 = \RedisPhp::setrange('olahraga', 4, 'an');

        // $data3 = \RedisPhp::mget(['olahraga', 'liburan', 'tv']);
        // \RedisPhp::mset(['key0' => 'value0', 'key1' => 'value1']);

        // $data3 = \RedisPhp::ttl('olahraga');
        // \RedisPhp::expire('olahraga', 60);
        // \RedisPhp::setex('olahraga' ,60, 'Berenang');

        // dd($data3);

        // \RedisPhp::pipeline(function ($pipe) {
        //     for ($i = 0; $i < 1000; $i++) {
        //         $pipe->set("key:$i", $i);
        //     }
        // });

        // \RedisPhp::multi();
        // \RedisPhp::set('key8', 'value 8');
        // \RedisPhp::set('key9', 'value 9');
        // \RedisPhp::set('key10', 'value 10');
        // \RedisPhp::discard();
        // \RedisPhp::exec();

        // dd(\RedisPhp::client('list'));
        // dd(\RedisPhp::client('101'));
        // dd(\RedisPhp::client('kill', '127.0.0.1:53933'));

        // return view('home');
    }
}
