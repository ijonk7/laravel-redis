<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CacheController extends Controller
{
    public function tesCache()
    {
        // $data2 = User::all();
        // Cache::set('dataUser2', $data2);
        // Cache::put('dataUser1', $data1);
        // Cache::put('angka1', 500);

        // $data2 = Cache::remember('dataUser3', 15, function () {
        //     return User::all();
        // });

        // Cache::store('redis')->put('bar', 'baz', 600);

        // $data2 = Cache::get('dataUser2');
        // $data2 = Cache::get('dataUser1', 'Data Default');

        // dd($data2);

        // foreach ($data2 as $value) {
        //     echo $value->name . "<br>";
        // }

        // if (Cache::has('dataUser1')) {
        //     return true;
        // } else {
        //     return false;
        // }

        // Cache::increment('angka1', 75);
        // Cache::decrement('angka1', 125);

        // Cache::pull('dataUser2');

        // Cache::add('dataUser2', $data2, 20);

        // Cache::forever('dataUser2', $data2);

        // Cache::tags(['people', 'artists'])->put('John', 'John 1', 900);
        // Cache::tags(['people', 'authors'])->put('Anne', 'Anne 1', 900);

        // $john = Cache::tags(['people', 'artists'])->get('John');
        // $anne = Cache::tags(['people', 'authors'])->get('Anne');

        // dump($john);
        // dump($anne);

        // Cache::tags('authors')->flush();
        // Cache::tags(['people', 'authors'])->flush();

        // $lock = Cache::lock('dataUser1', 10);

        // $data2 = Cache::get('dataUser1');

        // foreach ($data2 as $value) {
        //     echo $value->name . "<br>";
        // }

        // if ($lock->get()) {
        //     dump($lock);

            // $data2 = User::all();
            // Cache::put('dataUser2', $data2);

            // $lock->release();
        // }



        // dd($data2);
    }
}
